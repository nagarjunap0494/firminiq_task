package com.testapp

import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.downloader.*
import com.testapp.utils.Utils
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private var dirPath: String? = null
    private var mBooleanType: Boolean = true

    var downloadIdOne = 0
    var downloadIdTwo: Int = 0
    var downloadIdThree: Int = 0
    var downloadIdFour: Int = 0

    var mDownload : MutableLiveData<Int> = MutableLiveData()

    private val imageUrl1: String = "https://cdn.wallpapersafari.com/36/6/WCkZue.png"
    private val imageUrl2: String =
        "https://www.iliketowastemytime.com/sites/default/files/hamburg-germany-nicolas-kamp-hd-wallpaper_0.jpg"
    private val imageUrl3: String =
        "https://images.hdqwalls.com/download/drift-transformers-5-the-last-knight-qu-5120x2880.jpg"
    private val imageUrl4: String =
        "https://survarium.com/sites/default/files/calendars/survarium-wallpaper-calendar-february-2016-en-2560x1440.png"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val config = PRDownloaderConfig.newBuilder()
            .setDatabaseEnabled(true)
            .build()
        PRDownloader.initialize(applicationContext, config)

        dirPath = Utils.getRootDirPath(applicationContext)

        initLayout()

        getNoDownload().observe(this, Observer {
            if (it == 4) {
                button_sync.isEnabled = true
                button_async.isEnabled = true
                button_download.setText(R.string.finish)
            }
        })
    }

    private fun initLayout() {
        button_sync.setBackgroundColor(ContextCompat.getColor(this, R.color.light_blue));
        button_async.setBackgroundColor(ContextCompat.getColor(this, R.color.white));

        button_download.setOnClickListener {
            button_sync.isEnabled = false
            button_async.isEnabled = false

            if (mBooleanType) {
                startDownloadOne()
            } else {
                startDownloadOne()
                startDownloadTwo()
                startDownloadThree()
                startDownloadFour()
            }
        }
        button_sync.setOnClickListener {
            mBooleanType = true
            PRDownloader.cancel(downloadIdOne)
            PRDownloader.cancel(downloadIdTwo)
            PRDownloader.cancel(downloadIdThree)
            PRDownloader.cancel(downloadIdFour)

            button_download.setText(R.string.start)
            button_sync.setBackgroundColor(ContextCompat.getColor(this, R.color.light_blue));
            button_async.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        }

        button_async.setOnClickListener {
            mBooleanType = false
            PRDownloader.cancel(downloadIdOne)
            PRDownloader.cancel(downloadIdTwo)
            PRDownloader.cancel(downloadIdThree)
            PRDownloader.cancel(downloadIdFour)

            button_download.setText(R.string.start)
            button_sync.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
            button_async.setBackgroundColor(ContextCompat.getColor(this, R.color.light_blue));
        }
    }

    private fun startDownloadOne() {
        if (Status.RUNNING == PRDownloader.getStatus(downloadIdOne)) {
            PRDownloader.pause(downloadIdOne)
            return
        }

        progressBar1.isIndeterminate = true
        progressBar1.indeterminateDrawable.setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN)

        if (Status.PAUSED == PRDownloader.getStatus(downloadIdOne)) {
            PRDownloader.resume(downloadIdOne)
            return
        }

        Log.i("dirPath ==> ", dirPath!!)
        downloadIdOne = PRDownloader.download(imageUrl1, dirPath, "/image_one.png")
            .build()
            .setOnStartOrResumeListener {
                button_download.isEnabled = true
                progressBar1.isIndeterminate = false
                button_download.setText(R.string.pause)
            }
            .setOnPauseListener {
                button_download.setText(R.string.resume)
            }
            .setOnCancelListener {
                button_download.setText(R.string.start)
                progressBar1.progress = 0
                //textViewProgressOne.setText("")
                downloadIdOne = 0
                progressBar1.isIndeterminate = false
            }
            .setOnProgressListener { progress ->
                val progressPercent = progress.currentBytes * 100 / progress.totalBytes
                progressBar1.progress = progressPercent.toInt()
                progressBar1.isIndeterminate = false
            }
            .start(object : OnDownloadListener {
                override fun onDownloadComplete() {

                    mDownload.value?.let {
                        mDownload.value = it+1
                        Log.i("mDownload1111 ==> ", "" + mDownload.value)
                    }

                    if (mBooleanType) {
                        startDownloadTwo()
                    }
                    Log.i("dirPath ==> ", dirPath!! + "image_one.png")
                    imageView1.setImageBitmap(BitmapFactory.decodeFile(dirPath!! + "/image_one.png"))
                }

                override fun onError(error: Error) {
                    button_download.setText(R.string.start)
                    Toast.makeText(
                        applicationContext,
                        getString(R.string.some_error_occurred) + " " + "1",
                        Toast.LENGTH_SHORT
                    ).show()
                    //textViewProgressOne.setText("")
                    progressBar1.setProgress(0)
                    downloadIdOne = 0
                    //buttonCancelOne.setEnabled(false)
                    progressBar1.isIndeterminate = false
                    button_download.isEnabled = true
                }
            })
    }

    private fun startDownloadTwo() {
        if (Status.RUNNING == PRDownloader.getStatus(downloadIdTwo)) {
            PRDownloader.pause(downloadIdTwo)
            return
        }

        progressBar2.isIndeterminate = true
        progressBar2.indeterminateDrawable.setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN)

        if (Status.PAUSED == PRDownloader.getStatus(downloadIdTwo)) {
            PRDownloader.resume(downloadIdTwo)
            return
        }

        downloadIdTwo = PRDownloader.download(imageUrl2, dirPath, "/image_two.jpg")
            .build()
            .setOnStartOrResumeListener {
                progressBar2.isIndeterminate = false
            }
            .setOnPauseListener {
                button_download.setText(R.string.resume)
            }
            .setOnCancelListener {
                button_download.setText(R.string.start)
                progressBar2.progress = 0
                //textViewProgressOne.setText("")
                downloadIdTwo = 0
                progressBar2.isIndeterminate = false
            }
            .setOnProgressListener { progress ->
                val progressPercent = progress.currentBytes * 100 / progress.totalBytes
                progressBar2.progress = progressPercent.toInt()
                progressBar2.isIndeterminate = false
            }
            .start(object : OnDownloadListener {
                override fun onDownloadComplete() {
                    mDownload.value?.let {
                        mDownload.value = it+1
                        Log.i("mDownload2222 ==> ", "" + mDownload.value)

                    }
                    if (mBooleanType) {
                        startDownloadThree()
                    }
                    imageView2.setImageBitmap(BitmapFactory.decodeFile(dirPath!! + "/image_two.jpg"))
                }

                override fun onError(error: Error) {
                    button_download.setText(R.string.start)
                    Toast.makeText(
                        applicationContext,
                        getString(R.string.some_error_occurred) + " " + "1",
                        Toast.LENGTH_SHORT
                    ).show()
                    //textViewProgressOne.setText("")
                    progressBar2.setProgress(0)
                    downloadIdTwo = 0
                    //buttonCancelOne.setEnabled(false)
                    progressBar2.isIndeterminate = false
                    button_download.isEnabled = true
                }
            })
    }

    private fun startDownloadThree() {
        if (Status.RUNNING == PRDownloader.getStatus(downloadIdThree)) {
            PRDownloader.pause(downloadIdThree)
            return
        }

        progressBar3.isIndeterminate = true
        progressBar3.indeterminateDrawable.setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN)

        if (Status.PAUSED == PRDownloader.getStatus(downloadIdThree)) {
            PRDownloader.resume(downloadIdThree)
            return
        }

        downloadIdThree = PRDownloader.download(imageUrl3, dirPath, "/image_three.jpg")
            .build()
            .setOnStartOrResumeListener {
                button_download.isEnabled = true
                progressBar3.isIndeterminate = false
                button_download.setText(R.string.pause)
            }
            .setOnPauseListener {
                button_download.setText(R.string.resume)
            }
            .setOnCancelListener {
                button_download.setText(R.string.start)
                progressBar3.progress = 0
                //textViewProgressOne.setText("")
                downloadIdThree = 0
                progressBar3.isIndeterminate = false
            }
            .setOnProgressListener { progress ->
                val progressPercent = progress.currentBytes * 100 / progress.totalBytes
                progressBar3.progress = progressPercent.toInt()
                progressBar3.isIndeterminate = false
            }
            .start(object : OnDownloadListener {
                override fun onDownloadComplete() {
                    mDownload.value?.let {
                        mDownload.value = it+1
                        Log.i("mDownload3333 ==> ", "" + mDownload.value)
                    }

                    if (mBooleanType) {
                        startDownloadFour()
                    }

                    imageView3.setImageBitmap(BitmapFactory.decodeFile(dirPath!! + "/image_three.jpg"))
                }

                override fun onError(error: Error) {
                    button_download.setText(R.string.start)
                    Toast.makeText(
                        applicationContext,
                        getString(R.string.some_error_occurred) + " " + "1",
                        Toast.LENGTH_SHORT
                    ).show()
                    progressBar3.setProgress(0)
                    downloadIdThree = 0
                    progressBar3.isIndeterminate = false
                    button_download.isEnabled = true
                }
            })
    }

    private fun startDownloadFour() {
        if (Status.RUNNING == PRDownloader.getStatus(downloadIdFour)) {
            PRDownloader.pause(downloadIdFour)
            return
        }

        progressBar4.isIndeterminate = true
        progressBar4.indeterminateDrawable.setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN)

        if (Status.PAUSED == PRDownloader.getStatus(downloadIdFour)) {
            PRDownloader.resume(downloadIdFour)
            return
        }

        downloadIdFour = PRDownloader.download(imageUrl4, dirPath, "/image_four.png")
            .build()
            .setOnStartOrResumeListener {
                button_download.isEnabled = true
                progressBar4.isIndeterminate = false
                button_download.setText(R.string.pause)
            }
            .setOnPauseListener {
                button_download.setText(R.string.resume)
            }
            .setOnCancelListener {
                button_download.setText(R.string.start)
                progressBar4.progress = 0
                //textViewProgressOne.setText("")
                downloadIdFour = 0
                progressBar4.isIndeterminate = false
            }
            .setOnProgressListener { progress ->
                val progressPercent = progress.currentBytes * 100 / progress.totalBytes
                progressBar4.progress = progressPercent.toInt()
                progressBar4.isIndeterminate = false
            }
            .start(object : OnDownloadListener {
                override fun onDownloadComplete() {
                    mDownload.value?.let {
                        mDownload.value = it+1
                        Log.i("mDownload3333 ==> ", "" + mDownload.value)
                    }
                    imageView4.setImageBitmap(BitmapFactory.decodeFile(dirPath!! + "/image_four.png"))
                    if (mBooleanType) {
                        button_sync.isEnabled = true
                        button_async.isEnabled = true
                        button_download.setText(R.string.finish)
                    }
                }

                override fun onError(error: Error) {
                    button_download.setText(R.string.start)
                    Toast.makeText(
                        applicationContext,
                        getString(R.string.some_error_occurred) + " " + "1",
                        Toast.LENGTH_SHORT
                    ).show()
                    //textViewProgressOne.setText("")
                    progressBar4.progress = 0
                    downloadIdFour = 0
                    //buttonCancelOne.setEnabled(false)
                    progressBar4.isIndeterminate = false
                    button_download.isEnabled = true
                }
            })
    }

    private fun getNoDownload(): MutableLiveData<Int> {
        return mDownload
    }
}